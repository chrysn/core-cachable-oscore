# License

See the
[guidelines for contributions](https://github.com/git@gitlab.com:chrysn/core-cachable-oscore/blob/master/CONTRIBUTING.md).
