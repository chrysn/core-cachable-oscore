# Cachable OSCORE

This is the working area for the individual Internet-Draft, "Cachable OSCORE".

* [Editor's Copy](https://git@gitlab.com:chrysn.github.io/core-cachable-oscore/#go.draft-amsuess-core-cachable-oscore.html)
* [Individual Draft](https://tools.ietf.org/html/draft-amsuess-core-cachable-oscore)
* [Compare Editor's Copy to Individual Draft](https://git@gitlab.com:chrysn.github.io/core-cachable-oscore/#go.draft-amsuess-core-cachable-oscore.diff)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/git@gitlab.com:chrysn/core-cachable-oscore/blob/master/CONTRIBUTING.md).
